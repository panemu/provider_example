import 'package:flutter/material.dart';
import 'package:provider_example/ErrorData.dart';
import 'package:provider_example/Model2.dart';
import 'package:provider_example/ProviderStatus.dart';
import 'package:provider/provider.dart';

class PullToRefresh3 extends StatefulWidget {
  @override
  _PullToRefresh3State createState() => _PullToRefresh3State();
}

class _PullToRefresh3State extends State<PullToRefresh3> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController = new ScrollController();


  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        Provider.of<Model2>(context, listen: false).loadNextPage();
      }
    });
    super.initState();
  }

  void _showErrorDialog(ErrorData e) async {
    print('Error ' + DateTime.now().toString() + ' - ${e.error} - ' + e.stackTrace.toString());
    showDialog<void>(
      context: context, barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[Text(e.error.toString())],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print("$runtimeType build");

    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () {
          print("$runtimeType refresh");
          return Provider.of<Model2>(context, listen: false).loadFirstPage();
        },
        child: Consumer<Model2>(
          builder: (context, model, child) {
            if (model.status == ProviderStatus.reloading) {
              _refreshIndicatorKey.currentState.show();
            } else if (model.status == ProviderStatus.error) {
              WidgetsBinding.instance.addPostFrameCallback((_) => _showErrorDialog(model.errorData));
            }
            // print("$runtimeType build consumer");
            return Stack(
              children: [
                ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount: model.items.length + 1,
                  itemBuilder: (context, index) {
                    if (index == model.items.length) {
                      return Center(
                        child: model.status == ProviderStatus.loading_next_page ? CircularProgressIndicator() : Container(),
                      );
                    }
                    return ListTile(
                      title: Text("Page ${model.items[index].page} Item: ${model.items[index].index}"),
                      subtitle: Text("Model2"),
                    );
                  },
                  controller: _scrollController,
                ),
                Positioned(
                  right: 0,
                  child: RaisedButton(
                    onPressed: () {
                      _refreshIndicatorKey.currentState.show();
                    },
                    child: Text("Reload from this widget"),
                  ),
                )
              ],
            );
          },
        ),
    );
  }
}
