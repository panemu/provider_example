import 'package:flutter/material.dart';
import 'package:provider_example/Model2.dart';
import 'package:provider_example/MyModel.dart';
import 'package:provider_example/PullToRefresh.dart';
import 'package:provider_example/PullToRefresh3.dart';
import 'package:provider_example/PullToRefreshNoProvider.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  int _selectedIndex = 0;
  List<Widget> _page = [];

  @override
  void initState() {
    _page = [PullToRefresh(), PullToRefreshNoProvider(), PullToRefresh3()];

    super.initState();
  }

  void _onItemTapped(int idx) {
    setState(() {
      _selectedIndex = idx;
    });
  }

  @override
  Widget build(BuildContext context) {
    MyModel model = MyModel();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<MyModel>(create: (_) => MyModel()),
        ChangeNotifierProvider<Model2>(create: (_) => Model2()),
      ],
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Provider Example')),
          body: _page[_selectedIndex],
          floatingActionButton: _selectedIndex == 0 ? MyButton() : null,
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('With Provider'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.business),
                title: Text('No Provider'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.business),
                title: Text('Provider 2'),
              ),

            ],
            currentIndex: _selectedIndex,
            selectedItemColor: Colors.amber[800],
            onTap: _onItemTapped,
          ),
        ),
      ),
    );
  }
}

class MyButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("$runtimeType MyButton build");
    final myModel = Provider.of<MyModel>(context, listen: false);
    return RaisedButton(
      onPressed: () {
        myModel.requestLoadFirstPage();
      },
      child: Text("Reload from parent"),
    );
  }
}
