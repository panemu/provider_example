import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:provider_example/ErrorData.dart';
import 'package:provider_example/ProviderStatus.dart';

class MyModel with ChangeNotifier {

	List<String> _lstData = [];
	ProviderStatus _status = ProviderStatus.stable;
	ErrorData _errorData;
	int _dataPerPage = 15;
	int _page = 0;
	int _artificialError = 0;

	Future<void> loadFirstPage() async {
		_page = 0;
		await _loadData();
	}

	Future<void> _loadData() async {
		try {
			print("$runtimeType loading page: $_page");
			await Future.delayed(Duration(seconds: 2));

			if (++_artificialError % 5 == 0) {
				throw("Artificial error created every 5 requests to test error handling");
			}

			if (_page == 0) {
				_lstData.clear();
			}
			int pageCounter = _page + 1;
			for (int i = 1; i <= _dataPerPage; i++) {
				int idx = _page * _dataPerPage + i;
				_lstData.add("Page $pageCounter Item $idx");
			}
			_status = ProviderStatus.stable;
			_page++;
		} catch (e, s) {
			_status = ProviderStatus.error;
			_errorData = ErrorData(e, s);
		}
		notifyListeners();
	}

	void requestLoadFirstPage() {
		print("$runtimeType needReload");
		_status = ProviderStatus.reloading;
		_page = 0;
		notifyListeners();
	}

	void loadNextPage() {
		if (_status == ProviderStatus.loading_next_page) {
			return;
		}
		_status = ProviderStatus.loading_next_page;
		notifyListeners();
		_loadData();
	}

	ProviderStatus get status => _status;

	ErrorData get errorData => _errorData;

	UnmodifiableListView<String> get items => UnmodifiableListView(_lstData);
}