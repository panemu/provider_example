import 'package:provider_example/DtoExample.dart';
import 'package:provider_example/PaginatedListModel.dart';

class Model2 extends PaginatedListModel<DtoExample> {
  @override
  Future<List<DtoExample>> fetchData(int page) async {
    /**
		 *  CALL YOUR API HERE
		 *
		 * final map = await _api.get("notif/user?page=$page");
		 * List<dynamic> lst = map['list'];
		 * return lst.map((item) => DtoExample.fromJson(item)).toList();
		 *
		 *
		 */

    int dataPerPage = 15;
    await Future.delayed(Duration(seconds: 2));
    List<DtoExample> _lstData = [];
    int pageCounter = page + 1;
    for (int i = 1; i <= dataPerPage; i++) {
      int idx = page * dataPerPage + i;
      _lstData.add(DtoExample(pageCounter, idx));
    }
    return _lstData;
  }
}
