import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:provider_example/ErrorData.dart';
import 'package:provider_example/ProviderStatus.dart';

abstract class PaginatedListModel<T> with ChangeNotifier {

	List<T> _lstData = [];
	ProviderStatus _status = ProviderStatus.stable;
	ErrorData _errorData;

	int _page = 0;

	Future<void> loadFirstPage() async {
		_page = 0;
		await _loadData();
	}

	Future<void> _loadData() async {
		try {
			print("$runtimeType loading page: $_page");
			if (_page == 0) {
				_lstData.clear();
			}
			List<T> data = await fetchData(_page);
			_lstData.addAll(data);
			_status = ProviderStatus.stable;
			_page++;
		} catch (e, s) {
			_status = ProviderStatus.error;
			_errorData = ErrorData(e, s);
		}
		notifyListeners();
	}

	Future<List<T>> fetchData(int page);

	void requestLoadFirstPage() {
		print("$runtimeType needReload");
		_status = ProviderStatus.reloading;
		_page = 0;
		notifyListeners();
	}

	void loadNextPage() {
		if (_status == ProviderStatus.loading_next_page) {
			return;
		}
		_status = ProviderStatus.loading_next_page;
		notifyListeners();
		_loadData();
	}

	ProviderStatus get status => _status;

	ErrorData get errorData => _errorData;

	UnmodifiableListView<T> get items => UnmodifiableListView(_lstData);
}