import 'package:flutter/material.dart';
import 'package:provider_example/ErrorData.dart';
import 'package:provider_example/MyModel.dart';
import 'package:provider_example/ProviderStatus.dart';
import 'package:provider/provider.dart';

class PullToRefresh extends StatefulWidget {
  @override
  _PullToRefreshState createState() => _PullToRefreshState();
}

class _PullToRefreshState extends State<PullToRefresh> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  ScrollController _scrollController = new ScrollController();


  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        Provider.of<MyModel>(context, listen: false).loadNextPage();
      }
    });
    super.initState();
  }

  void _showErrorDialog(ErrorData e) async {
    print('Error ' + DateTime.now().toString() + ' - ${e.error} - ' + e.stackTrace.toString());
    showDialog<void>(
      context: context, barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[Text(e.error.toString())],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print("$runtimeType build");

    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () {
          print("$runtimeType refresh");
          return Provider.of<MyModel>(context, listen: false).loadFirstPage();
        },
        child: Consumer<MyModel>(
          builder: (context, model, child) {
            if (model.status == ProviderStatus.reloading) {
              _refreshIndicatorKey.currentState.show();
            } else if (model.status == ProviderStatus.error) {
              WidgetsBinding.instance.addPostFrameCallback((_) => _showErrorDialog(model.errorData));
            }
            // print("$runtimeType build consumer");
            return Stack(
              children: [
                ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount: model.items.length + 1,
                  itemBuilder: (context, index) {
                    if (index == model.items.length) {
                      return Center(
                        child: model.status == ProviderStatus.loading_next_page ? CircularProgressIndicator() : Container(),
                      );
                    }
                    return ListTile(
                      title: Text(model.items[index]),
                    );
                  },
                  controller: _scrollController,
                ),
                Positioned(
                  right: 0,
                  child: RaisedButton(
                    onPressed: () {
                      _refreshIndicatorKey.currentState.show();
                    },
                    child: Text("Reload from this widget"),
                  ),
                )
              ],
            );
          },
        ),
    );
  }
}
