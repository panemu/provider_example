import 'package:flutter/material.dart';
import 'package:provider_example/ErrorData.dart';
import 'package:provider_example/MyModel.dart';
import 'package:provider/provider.dart';

class PullToRefreshNoProvider extends StatefulWidget {
  @override
  _PullToRefreshNoProviderState createState() => _PullToRefreshNoProviderState();
}

class _PullToRefreshNoProviderState extends State<PullToRefreshNoProvider> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  List<String> _lstData = [];
  int _page = 1;
  ScrollController _scrollController = new ScrollController();
  int _artificialError = 0;
  bool _isLoading = false;
  int _dataPerPage = 15;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> _loadData() async {
    if (_isLoading) {
      return;
    }
    print("loadData");
    setState(() {
      _isLoading = true;
    });
    try {

      await Future.delayed(Duration(seconds: 2));

      if (++_artificialError % 5 == 0) {
        throw("Artificial error created every 5 requests to test error handling");
      }

      if (_page == 0) {
        _lstData.clear();
      }
      int pageCounter = _page + 1;
      for (int i = 1; i <= _dataPerPage; i++) {
        int idx = _page * _dataPerPage + i;
        _lstData.add("No Provider Page $pageCounter Item $idx");
      }

      _page++;
    } catch (e, s) {
      _showErrorDialog(ErrorData(e,s));
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => _refreshIndicatorKey.currentState.show());
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _loadData();
      }
    });
    super.initState();
  }

  void _showErrorDialog(ErrorData e) async {
    print('Error ' + DateTime.now().toString() + ' - ${e.error} - ' + e.stackTrace.toString());
     showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(e.error.toString())
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    print("$runtimeType build");

    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: () {
          _page = 0;
          return _loadData();
        },
        child: Stack(
          children: [
            ListView.builder(
              physics: AlwaysScrollableScrollPhysics(),
              itemCount: _lstData.length + 1,
              itemBuilder: (context, index) {

                if (index == _lstData.length) {
                  return Center(
                    child: _isLoading && _lstData.isNotEmpty ? CircularProgressIndicator() : Container(),
                  );
                }

                return ListTile(
                  title: Text(_lstData[index]),
                );
              },
              controller: _scrollController,
            ),
            Positioned(
              right: 10,
              top: 10,
              child: RaisedButton(
                onPressed: () {
                  _refreshIndicatorKey.currentState.show();
                },
                child: Text("Reload From This Widget"),
              ),
            )
          ],
        ));
  }
}
